import React, { useState, useEffect } from 'react'
import { queryGetAnimePagination, fetchAnime } from '../utils/Helper'
import { MovieCard } from './MovieCard'

export const MovieList = () => {
    const [movies, setMovies] = useState([])
    const [parameter, setParameter] = useState({ page: 1, perPage: 8 })
    const [isLoading, setIsLoading] = useState(false)

    const HandleOnSubmitSearch = (event) => {
        if (event.key === 'Enter') {
            const searchKey = event.target.value.length < 1 ? undefined : event.target.value
            setParameter({ ...parameter, search: searchKey })
        }
    }

    const fetchingData = () => {
        setIsLoading(true)

        const promises = fetchAnime(queryGetAnimePagination, parameter)
        promises.then(result => result.data)
            .then(data => setMovies(data))
            .catch(error => 'err' + error)
        if (movies.data !== undefined) {
            setMovies(movies.data)
            setIsLoading(false)
        }
    }
    // windows on load, menerima parameter 
    useEffect(() => {
        document.title = 'Anime Mdb - Daftar Anime';
        fetchingData();
        // eslint-disable-next-line
    }, [parameter])

    const paginate = (menu) => {
        let tempCurrentPage = parameter.page;
        setParameter({ page: tempCurrentPage, perPage: 8})
        if (menu === 'Selanjutnya') {
            tempCurrentPage++
        } else {
            tempCurrentPage--
        }
        setParameter({ page: tempCurrentPage, perPage: 8})
    }

    const pagination = () => {
        let menuNextPrev = ['Selanjutnya']
        if (parameter.page > 1) {
            menuNextPrev = ['Sebelumnya', 'Selanjutnya']
        }

        return (
            <nav>
                <ul className="pagination">
                    {menuNextPrev.map(menu => (
                        <li key={menu} className="page-item" onClick={event => paginate(menu)}>
                            <span className='page-link'style={{cursor:'pointer'}}>
                                {menu}
                            </span>
                        </li>
                    ))}
                </ul>
            </nav>
        )
    }

    return (

        <div className="movie-page">
            <div className="container">
                <div className="header">
                    <h1 className="heading">Anime Movie Database</h1>
                </div>
                <div className="row m-4">
                    <div className="col-md-12 col-sm-12">
                        <input type="text"
                            placeholder='Cari Anime/Manga Disini..'
                            className='form-control'
                            onKeyPress={event => HandleOnSubmitSearch(event)}
                        // style={{ width: '70%', borderRadius: '20px', padding: '10px', borderColor: 'gray', outline: 'none' }}
                        />
                    </div>
                </div>
                <div className="container">
                    <div className="row">
                        {movies !== undefined & movies.data !== undefined ? (
                            <>
                                {movies.data.Page.media.map((movie) => (
                                    <MovieCard movie={movie} key={movie.id} />
                                ))}
                            </>
                        ) : (
                            <h2 className="no-movies">{!isLoading ? "Memuat halaman..." : "Tidak Ada anime!"}</h2>
                        )}
                    </div>
                </div>
                {/* {captionPage ? captionPage : "Page 1"} */}
                {pagination()}
            </div>
        </div>)
}

// 