

import React from "react";
import { Link } from "react-router-dom";

export const MovieCard = anime => {
    let movie = anime.movie;
    return (
        <Link to="/detail" state={{ currentAnime: movie }} className="col-md-3 col-sm-12 mb-2" title={movie.description?.replace(/<\/?[^>]+(>|$)/g, "")}>
            
            <div className="movie-card" style={{ backgroundImage: `url(${movie.coverImage.large})` }}>
            <span className="badge bg-success m-2">{movie.seasonYear}</span>
            </div>
            <p style={{ textAlign: 'center', fontStyle: 'none' }}>{movie.title.english ? movie.title.english : movie.title.romaji}</p>
            
        </Link>
    );
};