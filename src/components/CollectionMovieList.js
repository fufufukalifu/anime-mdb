import React, { useState } from 'react'
import { MovieCard } from './MovieCard'

export const CollectionMovieList = () => {
    const [collection] = useState(localStorage.getItem("myCollection")
        ? JSON.parse(localStorage.getItem("myCollection"))
        : [])
    const [searchKeyword, setSearchKeyword] = useState('');

    const handleOnSubmitSearch = (value) => {
        console.log(searchKeyword);
    }

    return (
        <div className="movie-page">
            <div className="container">
                <div className="header">
                    <h1 className="heading">Anime Movie Database</h1>

                    <span className="count-pill">
                        {collection.length}
                    </span>
                </div>

                <div className="row m-4">
                    <div className="col-md-12 col-sm-12">
                        <input type="text"
                            placeholder='Cari Anime/Manga Disini..'
                            className='form-control'
                            onChange={event => event.target.value !== '' ? handleOnSubmitSearch(event.target.value) : setSearchKeyword("")}
                        />
                    </div>
                </div>

                <div className="container">
                    <div className="row">
                        {collection.length > 0 ? (
                            <>
                                {collection.map((movie) => (
                                    <MovieCard movie={movie} key={movie.id} />
                                ))}
                            </>
                        ) : (
                            <h2 className="no-movies">{collection.length > 0 ? "Memuat halaman..." : "Tidak Ada anime!"}</h2>
                        )}
                    </div>
                </div>
            </div>
        </div>)
}
