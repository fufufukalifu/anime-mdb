import React, { useEffect, useState } from 'react'
import { useLocation } from 'react-router-dom'
import { fetchAnime, queryGetAnime } from "../utils/Helper";

export const DetailMovie = () => {
    const { currentAnime } = useLocation().state
    const [anime] = useState(currentAnime);
    const [detailAnime, setDetailAnime] = useState([]);

    const [collection, setCollection] = useState(localStorage.getItem("myCollection")
        ? JSON.parse(localStorage.getItem("myCollection"))
        : [])
    const [isAdded, setIsAdded] = useState(false)

    const pushData = () => {
        if (isAdded) {
            const newCollection = collection.filter(item => item.id !== anime.id)
            setCollection(newCollection)
            localStorage.setItem('myCollection', JSON.stringify(newCollection))
            setIsAdded(false)
            alert(' Berhasil menghapus dari koleksi ')
        } else {
            const mergeOldNew = [...collection, anime];
            setCollection(mergeOldNew);
            localStorage.setItem('myCollection', JSON.stringify(mergeOldNew))
            setIsAdded(true)
            alert(' Berhasil menambahkan ke koleksi ')
        }
    }

    const fetchingData = () => {
        setIsAdded(collection.filter(item => item.id === anime.id).length > 0 ? true : false)
    }

    const fetchingAnime = () => {
        console.log('fetching online')
        let variable = { id: currentAnime.id };
        console.log(variable)
        const promises = fetchAnime(queryGetAnime, variable)
        promises.then(result => result.data)
            .then(data => setDetailAnime(data))
            .catch(error => 'err' + error)
        if (detailAnime.data !== undefined) {
            setDetailAnime(detailAnime.data)
        }
    }

    useEffect(() => {
        fetchingAnime();
        fetchingData();
        // eslint-disable-next-line
    }, [])

    return (
        <div>
            {detailAnime.data !== undefined ? (
                <>
                    <div className="row" style={{ height: '450px', backgroundImage: `url(${detailAnime.data.Media.bannerImage}) , linear-gradient(#eb01a5, #d13531)` }}>
                    </div>

                    <div className="container" style={{ marginTop: '-100px' }}>
                        <div className="row">
                            <div className='col-md-12 offset-md-2 col-sm-12 rounded-2' style={{ margin: '0 auto' }}>
                                <div className="card shadow">
                                    <div className="card-body">

                                        <h3>{anime.title.english ? anime.title.english : anime.title.romaji} / {anime.seasonYear}
                                        </h3>
                                        <div className="row">
                                            <div className="col-md-3 col-sm-12">
                                                <img src={detailAnime.data.Media.coverImage.large} alt={"Poster " + anime.title.english ? anime.title.english : anime.title.romaji} className="img-responseive rounded text-center mx-auto d-block" />
                                                {detailAnime.data.Media.genres.map((genre) => (
                                                    <span key={genre} className="badge bg-primary m-1">
                                                        {genre}
                                                    </span>
                                                ))}
                                                <p>
                                                    <span ></span>
                                                </p>

                                            </div>
                                            <div className="col-md-8 p-1 col-sm-12">

                                                <h5>Sinopsis <span className='badge bg-success'>{detailAnime.data.Media.averageScore}</span> </h5>
                                                <p dangerouslySetInnerHTML={{ __html: anime.description }}></p>
                                                
                                                <h5 className='mt-3'>External Link</h5>
                                                {detailAnime.data.Media.externalLinks.map((link) => (
                                                    <a target="_blank" href={link.url} key={link.id} className="m-1" rel="noreferrer">
                                                        {link.site}
                                                    </a>
                                                ))}

                                                <h5 className='mt-3'>Tags</h5>
                                                {detailAnime.data.Media.tags.map((tag) => (
                                                    <span className="badge bg-secondary rounded m-1">
                                                        {tag.name} {tag.rank}%
                                                    </span>
                                                ))}
                                            </div>
                                        </div>
                                    </div>

                                    <div className="card-footer">
                                        <button className={!isAdded ? "btn btn-success m-2" : "btn btn-warning m-2"} onClick={() => pushData(anime)}> {!isAdded ? "+ To" : "- From"}  Your Collection</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {/* // <div className="movie-grid">
                //     <p>{collection.length}</p>
                //     <p>{anime.id}</p>
                //     <p>{anime.title.english ? anime.title.english : anime.title.romaji}</p>
                //     <button onClick={() => pushData(anime)}> {!isAdded? "+" : "-"} </button>
                // </div> */}
                </>
            ) : (
                <div className="container">
                    loading...
                </div>
            )}
        </div>
    )
}
