import React from "react";
import { Link } from "react-router-dom";

export const Header = () => {
  return (
    <header>
      <div className="container">
        <div className="inner-content">
          <div className="brand">
            <Link to="/">AnimeMDB</Link>
          </div>

          <ul className="nav-links">
            <li>
              <Link to="/">Anime List</Link>
            </li>

            <li>
              <Link to="/collection-anime">Your Collection</Link>
            </li>

            <li>
              <Link to="/add" className="btn btn-main">
                + Collection List
              </Link>
            </li>
          </ul>
        </div>
      </div>
    </header>
  );
};
