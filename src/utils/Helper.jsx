export const queryGetAnimePagination = `query ($id: Int, $page: Int, $perPage: Int, $search: String) {
    Page(page: $page, perPage: $perPage) {
      pageInfo {
        total
        currentPage
        lastPage
        hasNextPage
        perPage
      }
      media(id: $id, search: $search) {
        id
        startDate {
          year
          month
          day
        }
        seasonYear
        title {
          romaji
          english
          native
        }
        genres
        description
        coverImage {
          large
        }
        
      }
    }
  }
  `;

  export const queryGetAnime = `
  query($id: Int) {
    Media(id: $id, type: ANIME) {
      trailer {
        id
        site
      }
      recommendations {
        edges {
          node {
            id
            mediaRecommendation {
              coverImage {
                extraLarge
                large
                medium
                color
              }
              title {
                romaji
                english
                native
                userPreferred
              }
              id
              type
              genres
              meanScore
              averageScore
              studios {
                edges {
                  node {
                    id
                    name
                    isAnimationStudio
                  }
                }
              }
              type
              popularity
              format
              episodes
              chapters
              season
              seasonYear
              status
              startDate {
                day
                month
                year
              }
              endDate {
                day
                month
                year
              }
            }
          }
        }
      }
      streamingEpisodes {
        title
        thumbnail
        url
        site
      }
      stats{
        scoreDistribution {
          score
          amount
        }
        statusDistribution {
          status
          amount
        }
      }
      staff {
        edges{
          node {
            image {
              large
              medium
            }
            name {
              first
              last
              full
              native
            }
            id
          }
          role
          id
        }
      }
      characters(sort: ID){
        edges {
          voiceActors(language: JAPANESE){
            image {
              medium
            }
            language
            name {
              full
              native
            }
            id
          }
          role
          node {
            image {
              medium
            }
            name {
              first
              last
              full
              native
            }
            id
          }
          id
        }
      }
      id
      bannerImage
      description(asHtml: true)
      coverImage {
        extraLarge
        large
        medium
        color
      }
      title {
        english
        romaji
        native
      }
      relations {
        edges {
          relationType
          node {
            coverImage {
              extraLarge
              large
              medium
              color
            }
            title {
              romaji
              english
              native
              userPreferred
            }
            id
            type
            genres
            meanScore
            averageScore
            studios {
              edges {
                node {
                  id
                  name
                  isAnimationStudio
                }
              }
            }
            type
            popularity
            format
            episodes
            chapters
            season
            seasonYear
            status
            startDate {
              day
              month
              year
            }
            endDate {
              day
              month
              year
            }
          }
          id
        }
      }
      genres
      meanScore
      averageScore
      type
      popularity
      format
      episodes
      duration
      startDate {
        year
        month
        day
      }
      endDate {
        year
        month
        day
      }
      season
      seasonYear
      status
      hashtag
      studios {
        edges {
          node {
            id
            name
            isAnimationStudio
          }
        }
      }
      source
      synonyms
      favourites
      tags {
        name
        isMediaSpoiler
        id
        rank
      }
      externalLinks {
        id
        site
        url
      }
    }
  }
`

export let parameterRequest = {
    page: 1,
    perPage: 10,
};

export async function fetchAnime(query, variables) {
  console.log(variables)
  const headers = {
   'Content-Type': 'application/json',
   'Accept': 'application/json',
  };

  const graphqlQuery = {
      "query": query,
      "variables": variables
  };

  const options = {
      "method": "POST",
      "headers": headers,
      "body": JSON.stringify(graphqlQuery)
  };
  
  const response = await fetch(anilistURL, options);
  const data = await response.json();
  
  return {data:data, error:data.errors}
}
export const anilistURL = 'https://graphql.anilist.co/';

export let setOptionFetching = (query, parameterRequest) => {
    return {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
        },
        body: JSON.stringify({
            query: query,
            variables: parameterRequest
        })
    }
};