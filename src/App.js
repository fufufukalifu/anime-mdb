import React from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import { Header } from "./components/Header";
import { DetailMovie } from "./components/DetailMovie";
import { MovieList } from "./components/MovieList";
import "./App.css";
import "./lib/font-awesome/css/all.min.css";
import { CollectionMovieList } from "./components/CollectionMovieList";


function App() {
  return (
    <BrowserRouter>
      <Header />
      <Routes>
        <Route exact path='/' element={<MovieList />} />
        <Route exact path='/detail' element={<DetailMovie />} />
        <Route exact path='/collection-anime' element={<CollectionMovieList />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;